package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import api.ISTSManager;

import model.vo.VORoute;
import model.vo.VOStop;
import model.data_structures.CircularLinkedList;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IList;


public class STSManager implements ISTSManager 
{

	@Override
	public void loadRoutes(String routesFile) 
	{	
		DoublyLinkedList<Route> routes = new DoublyLinkedList<Route>();
		
		File file = new File("/data/routes.txt");
		try 
		{
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			

			String linea = bufferedReader.readLine();
			String[] route = linea.split(",");
			
			for(int i = 0; i < route.length; i++)
			{
				String routeID = route[0];
				String agencyID = route[1];
				String routeShortName = route[2];
				String routeLongName = route[3];
				String routeDesc = route[4];
				String routeType = route[5];
				String routeUrl = route[6];
				String routeColor = route[7];
				String routeTextColor = route[8];
				
				routes.addLast(new Route(routeID, agencyID, routeShortName, routeLongName, routeDesc, routeType, routeUrl, routeColor, routeTextColor));
			}	
			
			fileReader.close();
			bufferedReader.close();
		} 
		
		catch (Exception e)
		{
			
			e.printStackTrace();
		}
	}

	@Override
	public void loadTrips(String tripsFile) 
	{
		CircularLinkedList<Trip> trips = new CircularLinkedList<Trip>();
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadStopTimes(String stopTimesFile) 
	{
		DoublyLinkedList<StopTime> stopTimes = new DoublyLinkedList<StopTime>();
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadStops(String stopsFile) 
	{
		CircularLinkedList<Stop> stops = new CircularLinkedList<Stop>();
		// TODO Auto-generated method stub
		
	}

	@Override
	public IList<VORoute> routeAtStop(String stopName) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) 
	{
		// TODO Auto-generated method stub
		return null;
	}

}
