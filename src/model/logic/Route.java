package model.logic;

public class Route 
{
	//
	// Atributes
	//
	
	private String routeID;
	private String agencyID;
	private String routeShortName;
	private String routeLongName;
	private String routeDesc;
	private String routeType;
	private String routeUrl;
	private String routeColor;
	private String routeTextColor;
	
	public Route(String routeID, String agencyID, String routeShortName, String routeLongName, String routeDesc, String routeType, String routeUrl, String routeColor, String routeTextColor )
	{
		setRouteID(routeID);
		setAgencyID(agencyID);
		setRouteShortName(routeShortName);
		setRouteLongName(routeLongName);
		setRouteDesc(routeDesc);
		setRouteType(routeType);
		setRouteUrl(routeUrl);
		setRouteColor(routeColor);
		setRouteTextColor(routeTextColor);
	}
	
	public String getRouteID() 
	{
		return routeID;
	}
	
	public void setRouteID(String routeID)
	{
		this.routeID = routeID;
	}
	
	public String getAgencyID() 
	{
		return agencyID;
	}
	
	public void setAgencyID(String agencyID) 
	{
		this.agencyID = agencyID;
	}
	
	public String getRouteShortName() 
	{
		return routeShortName;
	}
	
	public void setRouteShortName(String routeShortName) 
	{
		this.routeShortName = routeShortName;
	}
	
	public String getRouteDesc() 
	{
		return routeDesc;
	}
	
	public void setRouteDesc(String routeDesc) 
	{
		this.routeDesc = routeDesc;
	}
	
	public String getRouteLongName() 
	{
		return routeLongName;
	}
	
	public void setRouteLongName(String routeLongName) 
	{
		this.routeLongName = routeLongName;
	}
	
	public String getRouteType() 
	{
		return routeType;
	}
	
	public void setRouteType(String routeType) 
	{
		this.routeType = routeType;
	}
	
	public String getRouteUrl() 
	{
		return routeUrl;
	}
	
	public void setRouteUrl(String routeUrl) 
	{
		this.routeUrl = routeUrl;
	}
	
	public String getRouteColor() 
	{
		return routeColor;
	}
	
	public void setRouteColor(String routeColor) 
	{
		this.routeColor = routeColor;
	}
	
	public String getRouteTextColor() 
	{
		return routeTextColor;
	}
	
	public void setRouteTextColor(String routeTextColor) 
	{
		this.routeTextColor = routeTextColor;
	}
	
	
}
