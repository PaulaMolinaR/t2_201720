package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList <E> implements Iterable<E>
{
	//
	// Attributes
	//
	public Node first;
	public Node last;
	public int size;
	
	//
	// Constructor
	//
	
	public DoublyLinkedList()
	{
		size = 0;
	}
	

	private class Node
	{
		public Node previous;
		public Node next;
		public E element; 
		
	}
	
	//
	// Methods
	//
	
	/**
     * returns the size of the linked list
     * @return
     */
    public int size() 
    { 
    	return size; 
    }
    
    /**
     * return whether the list is empty or not
     * @return
     */
    public boolean isEmpty() 
    {
    	return size == 0; 
    }
    
   /**
     * adds element at the starting of the linked list
     * @param element
     */
    public void addFirst(E element) 
    {
    	//Push
    	Node oldFirst = first;
    	first = new Node();
    	first.element = element;
    	first.next = oldFirst;
    	size++;
    }
    
    /**
     * adds element at the end of the linked list
     * @param element
     */
    public void addLast(E element) 
    {
    	Node oldLast = last;
    	last = new Node();
    	last.element = element;
    	last.next = oldLast;
    	size++;	
    }
    
    /**
     * this method removes element from the start of the linked list
     * @return
     */
    public E deleteFirst() 
    {
    	//Pop
    	E element = first.element;
    	first = first.next;
    	size--;
    	return element;
    }
     
    /**
     * this method removes element from the end of the linked list
     * @return
     */
    public E deleteLast() 
    {
    	E element = last.element;
    	last = last.previous;
    	size--;
    	return element;
    }

    private class ListIterator implements Iterator<E>
    {
        private Node current = first;
        
        public boolean hasNext()
        {  
        	return current != null; 
        }
        
        public void remove() 
        {
        	
        }
        
        public E next()
        {
        	E element = current.element;
        	current = current.next;
        	return element;
		}
    }
	
	public Iterator<E> iterator()
    { 
		return new ListIterator(); 
	}

}
