package model.data_structures.test;

import junit.framework.TestCase;
import model.data_structures.DoublyLinkedList;

public class DoublyLinkedListTest<E> extends TestCase
{
	//
	// Attributes
	//
	
	/**
     * Clase donde se harán las pruebas.
     */
	private DoublyLinkedList<E> doublyLinkedList;
	
	
	//
	// Methods
	//
	
	/**
	 * Escenario 1: Crea una lista vacía.
	 */
	public void setupEscenario1()
	{
		try
		{
			doublyLinkedList = new DoublyLinkedList<E>();
		}
		catch(Exception e)
		{
			new Exception("No se debería generar el error " + e.getMessage());
		}
		
	}
	
	/**
	 * Escenario 2: Crea una lista con nodos. 
	 */
	public void setupEscenario2()
	{

		
	}
	
	public void sizeTest()
	{
		
	}
	
	public void addFirstTest()
	{
		
		
	}
	
	public void addLastTest()
	{
		
	}
	
	public void deleteFirstTest()
	{
		
	}
	
	public void deleteLastTest()
	{
		
	}

}
